import { StatusBar } from "expo-status-bar";
// Them thu vien font
import * as Font from "expo-font";
import React, { useState } from "react";
// Them cac trang giao dien
import Home from "./screens/home";
import About from "./screens/about";
// Them ham AppLoading
import { AppLoading } from "expo";

// Funtion load font
const getFonts = () => {
  return Font.loadAsync({
    "quicksand-regular": require("./assets/fonts/Quicksand-Regular.ttf"),
    "quicksand-bold": require("./assets/fonts/Quicksand-Bold.ttf"),
  });
};

export default function App() {
  // Bien check load font
  const [fontsLoaded, setFontsLoaded] = useState(false);

  // Khi load font thanh cong thi load noi dung
  if (fontsLoaded) {
    return <Home />;
  } else {
    return (
      <AppLoading startAsync={getFonts} onFinish={() => setFontsLoaded(true)} />
    );
  }

  return <Home />;
}
